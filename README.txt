Once installed, module is ready use.

On the node edit form select the "Publishing options" tab. There will be new
checkbox "Allow access to unpublished node via special link" (visible only if
"Published" checkbox is unchecked).

If you enable the option, the warning message will be shown on the node page,
like this:
This node is unpublished, but it can be accessed via the special link:
http://example.com/node/1?access_key=CxcCoLoGmHdldKMNYWzA2CMbTBrbhtB9vEBWnciNzVk

Via this link *any* visitor will be able to access this unpublished node.
